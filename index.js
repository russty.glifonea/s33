fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((data) => console.log(data))


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'GET',
	headers: {
		'Content-Type': 'application/json'
	},
})
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Created to do List',
		userID: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different data structur_",
		status: 'Pending',
		title: 'Updated To Do List Item',
		userID: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'DELETE',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		dateCompleted: "07/09/2021",
		status: 'COMPLETE',
		title: 'delectus aut autem',
		userID: 1
	})
})
.then((response) => response.json())
.then((data) => console.log(data))